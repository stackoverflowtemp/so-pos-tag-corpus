I/PRP
am/VBP
sad/JJ
I/PRP
can/MD
only/RB
give/VB
one/CD
upvote/NN
to/TO
this/DT
concise/JJ
and/CC
complete/JJ
question-with-answers-edited-in/NN
:)/UH

Suppose/VB
you/PRP
also/RB
want/VBP
to/TO
do/VB
ad/FW
hoc/FW
distribution/NN
through/IN
Jenkins/NNP
,/,
this/DT
necessitates/VBZ
that/IN
Jenkins/NNP
has/VBZ
access/NN
to/TO
a/DT
Distribution/NN
certificate/NN
,/,
and/CC
the/DT
team/NN
admin/NN
identity/NN
,/,
in/IN
addition/NN
to/TO
the/DT
provisioning/VBG
profiles/NNS
./.

Using/VBG
an/DT
exported/VBN
identity/NN
in/IN
a/DT
.cer/URI
file/NN
,/,
you/PRP
can/MD
programmatically/RB
import/VB
it/PRP
like/IN
so/RB
,/,
the/DT
-/C
A/C
switch/NN
is/VBZ
to/TO
allow/VB
all/DT
programs/NNS
access/NN
to/TO
this/DT
entry/NN
./.

Alternatively/RB
,/,
you/PRP
could/MD
use/VB
several/JJ
-/C
T/C
/path/to/program/URI
switches/VBZ
to/TO
allow/VB
codesign/NN
and/CC
xcodebuild/C
access/NN
./.
:/.

$/C
security/C
import/C
devcertificate/C
./C
cer/C
-/C
k/C
jenkins/C
./C
keychain/C
-/C
A/C

Of/IN
course/NN
,/,
we/PRP
should/MD
also/RB
have/VB
the/DT
Apple/NNP
WWDCRA/NNP
certificate/NN
,/,
imported/VBN
in/IN
pretty/RB
much/RB
the/DT
same/JJ
way/NN
:/.

$/C
security/C
import/C
AppleWWDRCA/C
./C
cer/C
-/C
k/C
jenkins/C
./C
keychain/C
-/C
A/C

However/RB
,/,
we/PRP
also/RB
need/VBP
the/DT
private/JJ
key/NN
for/IN
the/DT
devcertificate.cer/URI
./.

To/TO
do/VB
this/DT
,/,
you/PRP
need/VBP
to/TO
export/VB
the/DT
corresponding/JJ
private/JJ
key/NN
as/IN
a/DT
.p12/URI
key/NN
and/CC
set/VB
a/DT
password/NN
./.

Put/VB
it/PRP
somewhere/RB
you/PRP
can/MD
access/VB
it/PRP
from/IN
your/PRP$
Jenkins/NNP
shell/NN
,/,
unlock/VB
the/DT
keychain/NN
,/,
and/CC
import/VB
it/PRP
:/.

$/C
security/C
unlock-keychain/C
-/C
p/C
YourKeychainPass/C
jenkins/C
./C
keychain/C
$/C
security/C
import/C
devprivatekey/C
./C
p12/C
-/C
k/C
login/C
./C
keychain/C
-/C
P/C
ThePasswordYouSetWhenExporting/C
-/C
A/C

Importing/VBG
the/DT
distribution/NN
certificate/NN
works/VBZ
the/DT
same/JJ
way/NN
./.

I/PRP
do/VBP
n't/RB
know/VB
why/WRB
you/PRP
need/VBP
to/TO
unlock/VB
the/DT
keychain/NN
for/IN
importing/VBG
a/DT
.p12/URI
and/CC
not/RB
for/IN
a/DT
.cer/URI
,/,
but/CC
well/RB
./.

You/PRP
will/MD
also/RB
need/VB
access/NN
to/TO
the/DT
provisioning/VBG
profiles/NNS
,/,
I/PRP
will/MD
edit/VB
those/DT
instructions/NNS
into/IN
this/DT
post/NN
shortly/RB
./.
