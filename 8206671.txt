I/PRP
'm/VBP
running/VBG
the/DT
following/JJ
code/NN
..../.

package/JC
com/JC
./JC
dc/JC
;/JC

import/JC
android/JC
./JC
app/JC
./JC
Activity/JC
;/JC

import/JC
android/JC
./JC
content/JC
./JC
Intent/JC
;/JC

import/JC
android/JC
./JC
net/JC
./JC
Uri/JC
;/JC

import/JC
android/JC
./JC
os/JC
./JC
Bundle/JC
;/JC

import/JC
android/JC
./JC
util/JC
./JC
Log/JC
;/JC

import/JC
android/JC
./JC
webkit/JC
./JC
WebSettings/JC
;/JC

import/JC
android/JC
./JC
webkit/JC
./JC
WebView/JC
;/JC

public/JC
class/JC
DC/JC
extends/JC
Activity/JC
{/JC
/**/JC
Called/JC
when/JC
the/JC
activity/JC
is/JC
first/JC
created/JC
./JC
*//JC
@/JC
Override/JC
public/JC
void/JC
onCreate/JC
(/JC
Bundle/JC
savedInstanceState/JC
)/JC
{/JC
try/JC
{/JC
///JC
this/JC
is/JC
the/JC
code/JC
that/JC
I/JC
am/JC
surrounding/JC
in/JC
the/JC
try/catch/JC
block/JC
super/JC
./JC
onCreate/JC
(/JC
savedInstanceState/JC
)/JC
;/JC
///JC
init/JC
webview/JC
WebView/JC
DCWebView/JC
=/JC
(/JC
WebView/JC
)/JC
findViewById/JC
(/JC
R/JC
./JC
id/JC
./JC
webview/JC
)/JC
;/JC
///JC
when/JC
a/JC
link/JC
is/JC
clicked/JC
,/JC
use/JC
the/JC
WebView/JC
instead/JC
of/JC
opening/JC
a/JC
new/JC
browser/JC
DCWebView/JC
./JC
setWebViewClient/JC
(/JC
new/JC
MyWebViewClient/JC
(/JC
)/JC
{/JC
public/JC
void/JC
launchExternalBrowser/JC
(/JC
String/JC
url/JC
)/JC
{/JC
Intent/JC
intent/JC
=/JC
new/JC
Intent/JC
(/JC
Intent/JC
./JC
ACTION_VIEW/JC
,/JC
Uri/JC
./JC
parse/JC
(/JC
url/JC
)/JC
)/JC
;/JC
startActivity/JC
(/JC
intent/JC
)/JC
;/JC
}/JC
}/JC
)/JC
;/JC
///JC
enable/JC
javascript/JC
WebSettings/JC
webViewSettings/JC
=/JC
DCWebView/JC
./JC
getSettings/JC
(/JC
)/JC
;/JC
webViewSettings/JC
./JC
setJavaScriptEnabled/JC
(/JC
true/JC
)/JC
;/JC
}/JC
catch/JC
(/JC
Exception/JC
e/JC
)/JC
{/JC
///JC
this/JC
is/JC
the/JC
line/JC
of/JC
code/JC
that/JC
sends/JC
a/JC
real/JC
error/JC
message/JC
to/JC
the/JC
log/JC
Log/JC
./JC
e/JC
(/JC
"/JC
ERROR/JC
"/JC
,/JC
"/JC
ERROR/JC
IN/JC
CODE:/JC
"/JC
+/JC
e/JC
./JC
toString/JC
(/JC
)/JC
)/JC
;/JC
///JC
this/JC
is/JC
the/JC
line/JC
that/JC
prints/JC
out/JC
the/JC
location/JC
in/JC
///JC
the/JC
code/JC
where/JC
the/JC
error/JC
occurred/JC
./JC
e/JC
./JC
printStackTrace/JC
(/JC
)/JC
;/JC
}/JC
}/JC
}/JC

This/DT
stack/NN
trace/NN
is/VBZ
n't/RB
incredibly/RB
helpful/JJ
./.

I/PRP
'm/VBP
just/RB
learning/VBG
java/NNP
//CC
android/NNP
(/-LRB-
I/PRP
have/VBP
a/DT
background/NN
in/IN
PHP/NNP
)/-RRB-

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.892/ER
:/ER
ERROR/ER
//ER
ERROR/ER
(/ER
1032/ER
)/ER
:/ER
ERROR/ER
IN/ER
CODE/ER
:/ER
java/ER
./ER
lang/ER
./ER
NullPointerException/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.892/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
java/ER
./ER
lang/ER
./ER
NullPointerException/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.902/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
com/ER
./ER
dealclippings/ER
./ER
DealClippings/ER
./ER
onCreate/ER
(/ER
DealClippings/ER
./ER
java/ER
:/ER
24/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.902/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
Instrumentation/ER
./ER
callActivityOnCreate/ER
(/ER
Instrumentation/ER
./ER
java/ER
:/ER
1047/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.912/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
ActivityThread/ER
./ER
performLaunchActivity/ER
(/ER
ActivityThread/ER
./ER
java/ER
:/ER
1586/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.912/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
ActivityThread/ER
./ER
handleLaunchActivity/ER
(/ER
ActivityThread/ER
./ER
java/ER
:/ER
1638/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.922/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
ActivityThread/ER
./ER
access$1500/ER
(/ER
ActivityThread/ER
./ER
java/ER
:/ER
117/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.922/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
ActivityThread$H/ER
./ER
handleMessage/ER
(/ER
ActivityThread/ER
./ER
java/ER
:/ER
928/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.932/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
os/ER
./ER
Handler/ER
./ER
dispatchMessage/ER
(/ER
Handler/ER
./ER
java/ER
:/ER
99/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.932/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
os/ER
./ER
Looper/ER
./ER
loop/ER
(/ER
Looper/ER
./ER
java/ER
:/ER
123/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.942/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
android/ER
./ER
app/ER
./ER
ActivityThread/ER
./ER
main/ER
(/ER
ActivityThread/ER
./ER
java/ER
:/ER
3647/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.942/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
java/ER
./ER
lang/ER
./ER
reflect/ER
./ER
Method/ER
./ER
invokeNative/ER
(/ER
Native/ER
Method/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.952/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
java/ER
./ER
lang/ER
./ER
reflect/ER
./ER
Method/ER
./ER
invoke/ER
(/ER
Method/ER
./ER
java/ER
:/ER
507/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.952/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
com/ER
./ER
android/ER
./ER
internal/ER
./ER
os/ER
./ER
ZygoteInit$MethodAndArgsCaller/ER
./ER
run/ER
(/ER
ZygoteInit/ER
./ER
java/ER
:/ER
839/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.962/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
com/ER
./ER
android/ER
./ER
internal/ER
./ER
os/ER
./ER
ZygoteInit/ER
./ER
main/ER
(/ER
ZygoteInit/ER
./ER
java/ER
:/ER
597/ER
)/ER

11/ER
-/ER
20/ER
21/ER
:/ER
22/ER
:/ER
43.962/ER
:/ER
WARN/ER
//ER
System/ER
./ER
err/ER
(/ER
1032/ER
)/ER
:/ER
at/ER
dalvik/ER
./ER
system/ER
./ER
NativeStart/ER
./ER
main/ER
(/ER
Native/ER
Method/ER
)/ER
